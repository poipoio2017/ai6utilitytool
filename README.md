# AI6UtilityTool



## External tool setup

If you want to use the deployment options, install VNTranslationTools: https://github.com/arcusmaximus/VNTranslationTools

And arc_conv: https://github.com/amayra/arc_conv

And download: https://github.com/TesterTesterov/AI6WINArcTool for unpacking the mes.arc file.

Set the paths to VnTextPatch.exe and arc.exe in config.txt inside release folder.

## Initial setup

1) Use AI6WINArcTool to unpack mes.arc file from the main game folder
2) Use VNTranslationTools extractlocal to get json files from .mes files (from the unpacked folder)
3) Copy extracted .mes files to input\unpacked_mesarc
4) Copy the same .mes files to output\mes_files. That's a one time step because there are files other than .mes that are not processed by VNTranslationTools.
4) Copy extracted .json files (step2) to input\untranslated_jsons

## JSON format description

A typical line has a following format:

`  {
    "name": "俺",
    "message": "…。"
  },`

With "name" field being optional, depending if it's a dialogue or not.

## Option description

**Batch translate** - Inserts translations from input\translations folder to output\translated_jsons. Line by line, the text is replaced inside "message" field (see reference above). If there's a "name" field, the NameDictionary.txt file is checked for translation, if it's found, the translation is inserted. If not, the name is added to "not found names.txt" file.

**Save raw japanese** - Extracts japanese text from original jsons. If "with actor name" option is chosen, it appends the translated "name" before "message".

**Convert csv** - To be used with Translator++ export to csv option. Copy extracted .csv files to input\csv. The result lands in output\parsed_csv.

**Analyze text similarity** - Detects how many similar lines are there between each file, to detect related scenes. Input and output are in percentages.

**Fill scenes from stages** - Engine / Dev specific, the only files that should be translated are "end*" and "stage*" files. All other files are used for gallery mode (mainly "scene_*"). This is to automate the process of copying the translated text manually into individual files. IMPORTANT: you should probably do that after finishing the translation, otherwise there's no much point.
IMPORTANT2: after doing this, you need to manually copy the generated files into input\translations folder.

**Generate mes files** - Use external VNTranslationTools to convert output\translated_jsons into translated .mes files (output\mes_files)

**Pack mes.arc** - Use external arc_conv to create a translated mes.arc file (output\archive). After that step, you can overwrite mes.arc inside main game folder and play.

## General translation process

Typically you can start with saving raw japanese text and either translate it manually or import / paste into Translator++ (or any other tool). The option "with actors" is probably better to get context.

Next you create translated .txt files (e.g. stage00.txt) and put it into input\translation folder. IMPORTANT: the length must match the original .json file, otherwise a length mismatch error will be thrown. So if for instance you'd like to test a few lines, copy / paste the text from raw file and change a couple lines.

When you have a file ready, you can run options 1, 6, 7 and test. Don't forget to translate any unfound names ("not found names.txt") and add them to NameDictionary.txt.

Then just follow the steps above sequentially as you progress through translation.

## Example

Let's say I want to change translation of specific word from X to Y. 
1) E.g. use notepad++ to find/replace in files in input\translations folder (of course be careful to do it correctly)
2) Run steps 1, 6, 7.
3) Copy mes.arc to game folder, test.

## Final considerations

- There should be no manual changes done in files in output folder, they are all automatically regenerated.
- The only source of truth for translation is the input\translations folder. Meaning, all final changes should be done on .txt files there. Also, back up often.
