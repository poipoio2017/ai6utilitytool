#input paths
UntranslatedJsonFolderPath=input\untranslated_jsons
TranslationFolderPath=input\translations
CSVInputFolder=input\csv
UnpackedMesArc=input\unpacked_mesarc

#output paths
TranslatedMesFilesPath=output\mes_files
TranslatedArchiveFilePath=output\archive
TranslatedJsonsFolderPath=output\translated_jsons
RawOutputFolderPath=output\raw_all
RawWithNamesOutputFolderPath=output\rawnames_all
TranslatedStages=output\translated_stages
TranslationFromCsvFolderPath=output\parsed_csv

#External tools
VnTranslationToolsExePath=D:\Programs\VNTranslationTools-main\Build\Debug\VNTextPatch.exe
ArcPackerPath=D:\Programs\arc\arc.exe

#This is for converting stages to scenes. Probably should be left as is.
SceneStagePrefix=stage
SceneEndPrefix=end
